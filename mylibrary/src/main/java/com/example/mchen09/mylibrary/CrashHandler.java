package com.example.mchen09.mylibrary;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by MChen09 on 2016/4/13.
 * use:   Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(activity,url));
 */

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    private Context context;
    private static Context context1;
    private static CrashHandler instance = null;
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    private boolean isCustomLog = true;
    String url = "http://192.168.123.1/httpPostTest.php";

    public static CrashHandler getInstance(Context context, String _url) {
        context1 = context;

        if (instance == null) {
            instance = new CrashHandler(context1, _url);
        }
        return instance;
    }

    public CrashHandler(Context ctx, String _url) {
        context = ctx;
        context1 = ctx;
        url = _url;
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread t, Throwable e) {
        if (!handleException(e) && mDefaultHandler != null) {  //如果用户没有处理则让系统默认的异常处理器来处理
            mDefaultHandler.uncaughtException(t, e);
        } else {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ee) {
                Log.e("miyaki", "error : ", ee);
            }
//            ActivityStackControlUtil.finishProgram();// 结束所有Activity
            android.os.Process.killProcess(android.os.Process.myPid());// 退出程序
            System.exit(1);//Causes the VM to stop running and the program to exit with the given exit status
        }
    }

    private boolean handleException(final Throwable ex) {
        if (ex == null) {
            return false;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        ex.printStackTrace(printWriter);
        printWriter.close();
        String uncaughtException = stringWriter.toString();
        Log.e("miyaki", uncaughtException);
        StringBuilder report = new StringBuilder();
        report.append("Error: ").append(uncaughtException).append('\n');
        report.append("Locale: ").append(Locale.getDefault()).append('\n');
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi;
            pi = pm.getPackageInfo(context.getPackageName(), 0);
            report.append("Version: ").append(pi.versionName).append('\n');
            report.append("Package: ").append(pi.packageName).append('\n');
        } catch (Exception e) {
            Log.e("CustomExceptionHandler", "Error", e);
            report.append("Could not get Version information for ").append(context.getPackageName());
        }
        report.append("Brand: ").append(Build.BRAND).append('\n');
        report.append("Phone Model: ").append(Build.MODEL).append('\n');
        report.append("Android Version: ").append(Build.VERSION.RELEASE).append('\n');
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        report.append("Time: ").append(dateFormat.format(calendar.getTime())).append('\n');
//        String url = Global.regaineApiUrl + context.getString(R.string.apiAppExceptionlog);
        AQuery aq = new AQuery(context);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("Message", report.toString());
        aq.ajax(url, params, String.class, new AjaxCallback<String>() {
            @Override
            public void callback(String url, String json, AjaxStatus status) {
                Log.d("miyaki", getClass().getName() + ":" + status.getCode() + json.toString());
            }
        });
        return isCustomLog;
    }
}