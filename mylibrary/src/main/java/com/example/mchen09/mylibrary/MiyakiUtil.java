package com.example.mchen09.mylibrary;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

/**
 * Created by MChen09 on 2015/12/30.
 */
public class MiyakiUtil {

    public static  String getMessage(){
        return  "miyaki string";
    }

    public static void setview(Context cx){
        final Dialog dialog = new Dialog(cx);
        dialog.setContentView(R.layout.alert);
        dialog.setTitle("Title...");
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText("Android custom dialog example!");
        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageResource(R.mipmap.b);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //檔案單位
    private String toGB(double d) {
        return String.valueOf(d / 1024);
    }

    private String toMB(double d) {
        return String.valueOf(d / 1024 / 1024);
    }

    //超過1000回傳gb  小於1000就回傳mb
    private String toSize(double d) {
        double temp = d;
        int i = 0;
        int size = 1024;
        String unit = "";
        String sizeStr = "";
        while (temp > 1000) {
            temp = temp / size;
            i++;
        }
        DecimalFormat gbFormat = new DecimalFormat("#.##");
        DecimalFormat otherFormat = new DecimalFormat("#");
        Log.d("miyaki", getClass().getName() + "i:" + i);
        if (i == 0) {
            unit = "byte";
            sizeStr = otherFormat.format(temp);
        }
        if (i == 1) {
            unit = "KB";
            sizeStr = otherFormat.format(temp);
        }
        if (i == 2) {
            unit = "MB";
            sizeStr = otherFormat.format(temp);
        }
        if (i == 3) {
            unit = "GB";
            sizeStr = gbFormat.format(temp);
        }
        return sizeStr + unit;
    }



}
