package com.example.mchen09.mylibrary;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by MChen09 on 2016/4/13.
 */
public class UIUtil {

    public static void setTextEffect( int normal, int affect, View v) {
        TextView gridLayout = (TextView) v;
        MineSweeperOnTouchText gridLayoutListener = new MineSweeperOnTouchText(gridLayout, normal, affect);
        gridLayout.setOnTouchListener(gridLayoutListener);
    }

    public static void setImageEffect( int normal, int affect, View v) {
        ImageView gridLayout = (ImageView) v;
        MineSweeperOnTouchImage gridLayoutListener = new MineSweeperOnTouchImage(gridLayout, normal, affect);
        gridLayout.setOnTouchListener(gridLayoutListener);
    }


    public static void setButtonEffect( int normal, int affect, View v) {
        Button gridLayout = (Button) v;
        MineSweeperOnTouchButton gridLayoutListener = new MineSweeperOnTouchButton(gridLayout, normal, affect);
        gridLayout.setOnTouchListener(gridLayoutListener);

    }

    public void setUnderLine(View v) {
        TextView gridLayout = (TextView) v;
        gridLayout.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
    }

    //換顏色
    public static class MineSweeperOnTouchText implements View.OnTouchListener {
        private TextView gridLayout = null;
        private int normalResId;
        private int pressResId;

        public MineSweeperOnTouchText(TextView aGridLayout, int normalResId, int pressResId) {
            this.gridLayout = aGridLayout;
            this.normalResId = normalResId;
            this.pressResId = pressResId;
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                gridLayout.setTextColor(pressResId);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                gridLayout.setTextColor(normalResId);
            }
            return false;
        }
    }


    public static class MineSweeperOnTouchButton implements View.OnTouchListener {
        private Button gridLayout = null;
        private int normalResId;
        private int pressResId;

        public MineSweeperOnTouchButton(Button aGridLayout, int normalResId, int pressResId) {
            this.gridLayout = aGridLayout;
            this.normalResId = normalResId;
            this.pressResId = pressResId;
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                gridLayout.setBackgroundResource(pressResId);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                gridLayout.setBackgroundResource(normalResId);
            }
            return false;
        }
    }


    public static class MineSweeperOnTouchImage implements View.OnTouchListener {
        private ImageView gridLayout = null;
        private int normalResId;
        private int pressResId;

        public MineSweeperOnTouchImage(ImageView aGridLayout, int normalResId, int pressResId) {
            this.gridLayout = aGridLayout;
            this.normalResId = normalResId;
            this.pressResId = pressResId;

        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                gridLayout.setImageResource(pressResId);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                gridLayout.setImageResource(normalResId);
            }
            return false;
        }


    }
}
