package com.example.mchen09.mylibrary;

import android.app.Activity;
import android.content.Context;

/**
 * Created by MChen09 on 2016/4/13.
 */
public class ScreenUtil {


    public static int getScreenH(Activity activity){
       return  activity.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int getScreenW(Activity activity){
        return activity.getWindowManager().getDefaultDisplay().getHeight();
    }
    //px dp互轉
    public static int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }
}
