package com.example.mchen09.mylibrary;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

/**
 * Created by MChen09 on 2016/4/13.
 */
public class PermissionUtil  {

    private static int _REQUEST_CODE;
    private static IPermission _iPermission;
    private static String _PERMISSION_STRING;
    protected boolean checkPermission = false;

    public void setOnAllowListener(IPermission l) {

    }
    public interface IPermission{
        void onAllowPermission();
    }

    public  PermissionUtil(Activity activity,String PERMISSION_STRING, int REQUEST_CODE, IPermission iPermission) {
        checkPermission = false;
        Log.d("miyaki", getClass().getName() + ":" + this + " " + checkPermission);
        _iPermission = iPermission;
        _REQUEST_CODE = REQUEST_CODE;
        if (ContextCompat.checkSelfPermission(activity, PERMISSION_STRING) == PackageManager.PERMISSION_GRANTED) {
            Log.d("miyaki", getClass().getName() + " 允許 :");
            _iPermission.onAllowPermission();
        } else if (!checkPermission) {
            checkPermission = true;
            ActivityCompat.requestPermissions(activity, new String[]{PERMISSION_STRING}, REQUEST_CODE);
            Log.d("miyaki", getClass().getName() + " no允許 :");
        } else {
            // Wait for permission result
        }
    }

   /*public void checkPermission(String PERMISSION_STRING, int REQUEST_CODE, IPermission iPermission) {
        checkPermission = false;
        Log.d("miyaki", getClass().getName() + ":" + this + " " + checkPermission);
        _iPermission = iPermission;
        _REQUEST_CODE = REQUEST_CODE;
        if (ContextCompat.checkSelfPermission(this.activity, PERMISSION_STRING) == PackageManager.PERMISSION_GRANTED) {
            Log.d("miyaki", getClass().getName() + " 允許 :");
            _iPermission.onAllowPermission();
        } else if (!checkPermission) {
            checkPermission = true;
            ActivityCompat.requestPermissions(this.activity, new String[]{PERMISSION_STRING}, REQUEST_CODE);
            Log.d("miyaki", getClass().getName() + " no允許 :");
        } else {
            // Wait for permission result
        }
    }*/

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        doNext(requestCode, grantResults);
        Log.d("miyaki", getClass().getName() + " grantResults :" + grantResults);
    }

    private void doNext(int requestCode, int[] grantResults) {
        if (requestCode == _REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("miyaki", getClass().getName() + " 允許 :");
                Log.d("miyaki", getClass().getName() + ":" + this);
                _iPermission.onAllowPermission();
            } else {
                Log.d("miyaki", getClass().getName() + " 不允許 :");
            }
        }
    }



}
