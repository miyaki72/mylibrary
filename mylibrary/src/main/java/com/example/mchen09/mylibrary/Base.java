package com.example.mchen09.mylibrary;

import android.app.Activity;
import android.content.Context;
import android.text.GetChars;
import android.view.View;

/**
 * Created by MChen09 on 2016/4/13.
 */
public class Base {
    public Base() {
    }
    public void setTextEffect(int normal, int affect, View v) {
        UIUtil.setTextEffect(normal, affect, v);
    }
    public void setImageEffect(int normal, int affect, View v) {
        UIUtil.setImageEffect(normal, affect, v);
    }
    public void setButtonEffect(int normal, int affect, View v) {
        UIUtil.setButtonEffect(normal, affect, v);
    }
    //如果要給外面實做(alertnoconnect or permission)
    //寫成像aquery ajax的那種callback
    public void checkconnect(Activity activity,NetworkUtil.IInternetListener iInternetListener){
        NetworkUtil.alertNoConnect(activity, iInternetListener);

    }

}
