package com.example.mchen09.mylibrary;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by MChen09 on 2016/4/13.
 */
public class NetworkUtil {

    /*   NetworkUtil.alertNoConnect(this,new NetworkUtil.IInternetListener() {
            @Override
            public void onInternetOK() {
                Log.d("miyaki",getClass().getName()+"connect:");
            }
        });*/
    //這種寫法沒跑建構式  可是又要先new一個dialog**********************************************************
    //連結式寫法ex:dialog.bulder

    public NetworkUtil(Activity activity) {
        connectDialog = new Dialog(activity, R.style.CustomDialog2);
    }

    public interface IInternetListener {
        void onInternetOK();
    }

    //檢查網路帶一個重整按鈕
    public static void alertNoConnect(final Activity activity, final IInternetListener connectListener) {
        connectDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        connectDialog.setContentView(R.layout.no_connect);
        connectDialog.setCancelable(false);
        connectDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    Log.d("miyaki", this.getClass().getName() + ":按了返回鍵2");
                    Intent MyIntent = new Intent(Intent.ACTION_MAIN);
                    MyIntent.addCategory(Intent.CATEGORY_HOME);
                    activity.startActivity(MyIntent);
                }
                return false;
            }
        });
//        ImageView gridLayout = (ImageView) connectDialog.findViewById(R.id.reConnect);
//        MineSweeperOnTouch gridLayoutListener = new MineSweeperOnTouch(gridLayout, R.mipmap.reload, R.mipmap.reload_affect);
//        gridLayout.setOnTouchListener(gridLayoutListener);
        ImageView reconnect = (ImageView) connectDialog.findViewById(R.id.reConnect);
        //一進頁面要判斷HaveInternet
        if (haveInternet(activity)) {
            if (connectDialog.isShowing()) {
                CloseConnectAlert();
//                if (activity.getPackageName().equals("tw.com.regaine.confidence.MainActivity")) {
////                    ((activity) ()).LossHairClick(lossnum);
//                }
            }
            connectListener.onInternetOK();
        } else {
            connectDialog.show();
        }
        reconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertNoConnect(activity, connectListener);
            }
        });
    }

    static Dialog connectDialog;

    public static void CloseConnectAlert() {
        connectDialog.dismiss();
    }

    //有無網路
    public static boolean haveInternet(Activity activity) {
        boolean result = false;
        ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            result = false;
        } else {
            if (!info.isAvailable()) {
                result = false;
            } else {
                result = true;
            }
        }
        return result;
    }
}
