package com.example.mchen09.mylibrary;

import android.Manifest;
import android.content.Intent;
import android.provider.DocumentsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends RootActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new PermissionUtil(this, Manifest.permission.READ_EXTERNAL_STORAGE, 22, new PermissionUtil.IPermission() {
            @Override
            public void onAllowPermission() {
                Log.d("miyaki",getClass().getName()+"外面接收允許:");
            }
        });


        NetworkUtil.alertNoConnect(this,new NetworkUtil.IInternetListener() {
            @Override
            public void onInternetOK() {
                Log.d("miyaki",getClass().getName()+"connect:");
            }
        });


        rbase.checkconnect(this, new NetworkUtil.IInternetListener() {
            @Override
            public void onInternetOK() {
                Log.d("miyaki",getClass().getName()+" 網路callback :");
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
